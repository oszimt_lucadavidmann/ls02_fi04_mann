package kreis;

import java.awt.Point;

public class RechteckTest {
	
		public static void main(String[] args) {
			
			Rechteck r = new Rechteck(2,2,new Point(20,20));
			System.out.println("Flaeche: " + r.getFlaeche());
	        System.out.println("Umfang: " + r.getUmfang());
	        System.out.printf("Diagonale: %.02f", r.getDiagonale());
		}	
	
}
