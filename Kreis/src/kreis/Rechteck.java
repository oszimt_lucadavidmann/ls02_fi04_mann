package kreis;

import java.awt.Point;

public class Rechteck {
	private double x;
	private double y;
	private Point mittelpunkt;
    
    public Rechteck(double x, double y, Point mittelpunkt) {
    	this.setX(x);
    	this.setY(y);
    	this.setMittelpunkt(mittelpunkt);
    }

	public void setMittelpunkt(Point mittelpunkt) {
		this.mittelpunkt = mittelpunkt;
	}
	
	public Point getMittelpunkt() {
		return mittelpunkt;
	}

	public double getY() {
		return y;
	}

	public double getX() {
		return x;
	}
        
    public void setX(double x) {
        this.x = x;
        if(this.x < 0) {
            this.x = 0;
        }
    }
        
    public void setY(double y) {
        this.y = y;
        if(this.y < 0) {
            this.y = 0;
        }
    }
    public double getUmfang() {
        return (2 * this.x) + (2 * this.y);
    }
    
    public double getFlaeche() {
        return this.x * this.y;
    }
    
    public double getDiagonale() {
    	return Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y, 2));
    }

}