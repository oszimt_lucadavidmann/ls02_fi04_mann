package raumschiff;

import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	private String schiffsName;
	private int schildeInProzent;
	private int huelleInProzent;
	private int energieversorgungInProzent;
	private int lebensErhaltungsSystemeInProzent;
	private int photonenTorpedoAnzahl;
	private int androidenAnzahl;
	public ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungen;
/**
 * erzeugt ein Raumschiff objekt ohne parameter
 */
	public Raumschiff() {
	}
	
/**
 * Erzeugt ein Raumschiff objekt mit Parameter
 * @param schiffsName gibt den schiffs Namen an
 * @param schildeInProzent gibt die Schlilde in Prozent an
 * @param huelleInProzent gibt die Hülle in Prozent an
 * @param energieversorgungInProzent gibt die Energieversorgung in Prozent an
 * @param lebensErhaltungsSystemeInProzent gibt das Lebenserhaltungssystem in Prozent an
 * @param photonenTorpedoAnzahl gibt die Anzahl der Photonen Torpedo an
 * @param androidenAnzahl gibt die anzahl der Androiden an
 */
	public Raumschiff(String schiffsName, int schildeInProzent, int huelleInProzent,
			int energieversorgungInProzent, int lebensErhaltungsSystemeInProzent, int photonenTorpedoAnzahl,
			int androidenAnzahl) {
		this.schiffsName = schiffsName;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.lebensErhaltungsSystemeInProzent = lebensErhaltungsSystemeInProzent;
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungen = new ArrayList<Ladung>();
	}
/**
 * Fügt den Objekt eine Ladung hizu
 * @param ladung gibt die zu hinzufügende Ladung an
 */
	public void addLadung(Ladung ladung) {
		this.ladungen.add(ladung);
	}
	/**
	 * Gibt auskunpft über den Zustand des Schiffes auf die Konsole
	 */
	public void zustandAusgeben() {
		System.out.println("********** Zustand: " + this.schiffsName +  "**********");
		System.out.println("Schilde: " + this.schildeInProzent + "%");
		System.out.println("Huelle: " + this.huelleInProzent + "%");
		System.out.println("Energie: " + this.energieversorgungInProzent + "%");
		System.out.println("Lebenserhaltung: " + this.lebensErhaltungsSystemeInProzent + "%");
		System.out.println("Photonen Torpedos: "+ this.photonenTorpedoAnzahl);
		System.out.println("Androiden Anzahl: " + this.androidenAnzahl);
	}
	/**
	 * gibt das Ladungsverzeichnis auf die Konsole aus
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("**********" + this.schiffsName + "- Ladungen" +  "**********");
		for(Ladung ladung : this.ladungen) {
			System.out.println(ladung.getBezeichnung() + ": " + ladung.getMenge());
		}
	}
	/**
	 * räumt das Ladungsverzeichnis auf
	 * löscht leere einträge aus den Ladungsverzeichnis
	 */
	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i < this.ladungen.size(); i++) {
			Ladung ladung = this.ladungen.get(i);
			
			if(ladung.getMenge() == 0) {
				this.ladungen.remove(i);
			}
		}
	}
	/** lädt Torpedos aus der Ladung in die Torpedoröhren
	 * @param torpedosAnzahl anzahl der zu ladenden Torpedos
	 */
	public void photonenTorpedosEinsetzen(int torpedosAnzahl) {
		Ladung ladung = null;
		for(Ladung l : this.ladungen) {
			if(l.getBezeichnung().equalsIgnoreCase("Photonentorpedo")) {
				ladung = l;
				break;
			}
		}
		
		if(ladung != null) {	
		if(torpedosAnzahl > ladung.getMenge()) {
			torpedosAnzahl = ladung.getMenge();
		}
		
		ladung.setMenge(ladung.getMenge()- torpedosAnzahl);
		this.setPhotonenTorpedoAnzahl(torpedosAnzahl);
		
		System.out.println("[" + torpedosAnzahl + "] Photonentorpedos eingesetzt");
		
		}else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
		
	}
	
	/**
	 * Repariert ausgewählte teile des Schiffes 
	 * @param androiden gibt die an anzahl der Andoriden an die die Reperatur ausführen
	 * @param schild sagt ob die schilde repariert werden sollen
	 * @param huelle sagt ob die huelle repariert werden sollen
	 * @param energie sagt ob die energieversorgung repariert werden sollen
	 * @param leben sagt ob die lebenserhaltungssysteme repariert werden sollen
	 */
	public void reparaturAndroidenEinsetzen(int androiden,boolean schild,boolean huelle, boolean energie, boolean leben){
		if(androiden > this.androidenAnzahl) {
			androiden = this.androidenAnzahl;
		}
		
		Random random = new Random();
		
		int anzahlStrukturen = 0;
		if(schild) anzahlStrukturen++;
		if(huelle) anzahlStrukturen++;
		if(energie) anzahlStrukturen++;
		if(leben) anzahlStrukturen++;
		
		int repaired = (random.nextInt(101) * androidenAnzahl/anzahlStrukturen);
		
		if(schild) {
			this.setSchildeInProzent(this.schildeInProzent + repaired);
		}
		
		if(huelle) {
			this.setHuelleInProzent(this.huelleInProzent + repaired);
		}
		
		if(energie) {
			this.setEnergieversorgungInProzent(this.energieversorgungInProzent + repaired);
		}
		
		if(leben) {
			this.setLebensErhaltungsSystemeInProzent(this.lebensErhaltungsSystemeInProzent + repaired);
		}		
	}
	
	/**
	 * Schickt eine Nachricht an die Crew
	 * @param nachricht ist die zu übertragende Nachricht
	 */
	public void nachrichtAnAlle(String nachricht) {
		this.broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}
		/**
		 * feuert eine Photonen Torpedos auf ein ziel ab
		 * wenn es keine Torpedos gibt dann wird an die Crew eine Nachricht mit Click gegeben
		 * @param ziel
		 */
	public void photnentorpedosAbschießen(Raumschiff ziel) {
		if (this.photonenTorpedoAnzahl <= 0) {
			this.nachrichtAnAlle("-=*Click*=-");
		} else {
			this.photonenTorpedoAnzahl --;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			ziel.trefferVermerken();
		}
	}
	/**
	 * feuert die Phaser Kanone auf ein ziel ab
	 * Wenn nicht genügend Energie vorhanden ist dann wird die Crew benachrichtig das es nicht geklappt hat
	 * @param ziel
	 */
	public void phaserKanonenAbschießen(Raumschiff ziel) {
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaser Kanonen abgeschossen");
			ziel.trefferVermerken();
		}
	}
	/**
	 * gibt das log Buch aus
	 */
	public void logBuchAusgeben() {
		System.out.println("####### [Logbuch] #######");
		for (String nachricht: this.broadcastKommunikator) {
			System.out.println(nachricht);
		}
	}
	/**
	 * Vermerkt einen Treffer auf dem Raumschiff und verändert die Werte des Schiffes
	 */
	private void trefferVermerken() {
		System.out.println(this.schiffsName + "wurde getroffen!");
		
		this.setSchildeInProzent(this.schildeInProzent - 50);
		if(this.schildeInProzent == 0) {
			this.setHuelleInProzent(this.huelleInProzent - 50);
			this.setEnergieversorgungInProzent(this.energieversorgungInProzent - 50);
			
			if(this.huelleInProzent == 0) {
				this.setLebensErhaltungsSystemeInProzent(0);
				this.nachrichtAnAlle("Lebenserhaltungssysteme sind vernichtet worden");
			}
		}
	}	
/**
 * @return Schiffsname
 */
	public String getSchiffsName() {
		return schiffsName;
	}
/**
 * @param schiffsName setzt den Schiffsnamen
 */
	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}
/**
 * @return Schilde in Prozent
 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
/**
 * @param schildeInProzent Setzt die Schilde und check ob sie über 0% ist
 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
		if(this.schildeInProzent < 0) {
			this.schildeInProzent = 0;
		}
	}
/**
 * @return Huelle in Prozent
 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
/**
 * 
 * @param huelleInProzent Setzt die Hülle und check ob sie über 0% ist
 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
		if(this.huelleInProzent < 0) {
			this.huelleInProzent = 0;
		}
	}
/**
 * @return Energieversorgung In Prozent
 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
/**
 * @param energieversorgungInProzent Setzt die Energieversorgung und check ob sie über 0% ist
 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
		if(this.energieversorgungInProzent < 0) {
			this.energieversorgungInProzent = 0;
		}
	}
/**
 * @return lebenserhalungssysteme in Prozent
 */
	public int getLebensErhaltungsSystemeInProzent() {
		return lebensErhaltungsSystemeInProzent;
	}
/**
 * @param lebensErhaltungsSystemeInProzent Setzt die Lebenserhaltung und check ob sie über 0% ist
 */
	public void setLebensErhaltungsSystemeInProzent(int lebensErhaltungsSystemeInProzent) {
		this.lebensErhaltungsSystemeInProzent = lebensErhaltungsSystemeInProzent;
		if(this.lebensErhaltungsSystemeInProzent < 0) {
			this.lebensErhaltungsSystemeInProzent = 0;
		}
	}
/**
 * @return Photonen Torpedo Anzahl
 */
	public int getPhotonenTorpedoAnzahl() {
		return photonenTorpedoAnzahl;
	}
/**
 * @param photonenTorpedoAnzahl Setzt die Photonen Torpedo Anzahl und check ob sie über 0 ist
 */
	public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
		if(this.photonenTorpedoAnzahl < 0) {
			this.photonenTorpedoAnzahl = 0;
		}
	}
/**
 * @return androiden Anzahl
 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
/**
 * @param androidenAnzahl Setzt die Andoriden Anzahl und check ob sie über 0 ist
 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
		if(this.androidenAnzahl < 0) {
			this.androidenAnzahl = 0;
		}
	}
/**
 * @return broadcast Kommunikator
 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
/*
 * Setzt die nachrichten für den Broadcast Kommunikator
 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
/**
 * @return gibt die Ladungen zurück
 */
	public ArrayList<Ladung> getLadungen() {
		return ladungen;
	}
/*
 * Setzt die ladungen 
 */
	public void setLadungen(ArrayList<Ladung> ladungen) {
		this.ladungen = ladungen;
	}

}
