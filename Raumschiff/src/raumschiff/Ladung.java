package raumschiff;

public class Ladung {
	
	private String bezeichnung;
	
	private int menge;
	/**
	 * Erstellt ein Objekt mit Parametern
	 * @param bezeichnung gibt die Ladungsbezeichnung an
	 * @param menge ist die Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.setMenge(menge);
	}
	/**
	 * @return gibt die Bezeichnung zurück
	 */
	public String getBezeichnung() {
		return this.bezeichnung;
	}
/**
 * @param bezeichnung setzt die Ladungsbezeichnung
 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
/**
 * @return gibt die Menge zurück
 */
	public int getMenge() {
		return this.menge;
	}
/**
 * @param menge setzt die Menge und schaut ob sie über 0 ist
 */
	public void setMenge(int menge) {
		this.menge = menge;
		if(this.menge < 0) {
			this.menge = 0;
		}
	}
}
