package raumschiff;

public class RaumschiffTest {

    public static void main(String[] args) {
    	
    	// Vulkanierschiff
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 50, 80, 100, 0, 5);
        Ladung forschungssonde = new Ladung("Forschungssonde", 35);
        Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
        
        vulkanier.addLadung(forschungssonde);
        vulkanier.addLadung(photonentorpedo);
    	        
        // Romulanerschiff
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
        Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
        Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
        Ladung roteMaterie = new Ladung("Rote Materie", 2);
        
        romulaner.addLadung(borgSchrott);
        romulaner.addLadung(plasmaWaffe);
        romulaner.addLadung(roteMaterie);
        
     // Klingonenschiff
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
        Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
        Ladung batleth = new Ladung("Bat'leth Klingen Schwert", 200);
        
        klingonen.addLadung(schneckensaft);
        klingonen.addLadung(batleth);
        
        // Funktionsaufrufe
        
        klingonen.photnentorpedosAbschießen(romulaner);
        System.out.println();
        romulaner.phaserKanonenAbschießen(klingonen);
        System.out.println();
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        System.out.println();
        klingonen.zustandAusgeben();
        System.out.println();
        klingonen.ladungsverzeichnisAusgeben();
        System.out.println();
        vulkanier.reparaturAndroidenEinsetzen(5, true, true, true, false);
        System.out.println();
        vulkanier.photonenTorpedosEinsetzen(3);
        vulkanier.ladungsverzeichnisAufraeumen();
        System.out.println();
        klingonen.photnentorpedosAbschießen(romulaner);
        System.out.println();
        klingonen.photnentorpedosAbschießen(romulaner);
        System.out.println();
        klingonen.zustandAusgeben();
        System.out.println();
        klingonen.ladungsverzeichnisAusgeben();
        System.out.println();
        romulaner.zustandAusgeben();
        System.out.println();
        romulaner.ladungsverzeichnisAusgeben();
        System.out.println();
        vulkanier.zustandAusgeben();
        System.out.println();
        vulkanier.ladungsverzeichnisAusgeben();
    }
}
