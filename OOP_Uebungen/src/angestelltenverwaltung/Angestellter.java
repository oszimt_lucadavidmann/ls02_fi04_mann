package angestelltenverwaltung;

public class Angestellter {

	private String name;
	private double gehalt;

	public Angestellter() {
	}

	public Angestellter(String name) {
		this.name = name;
	}
	
	public Angestellter(String name, double gehalt) {
		this.name = name;
		this.gehalt = gehalt;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getGehalt() {
		return gehalt;
	}

	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}

}
