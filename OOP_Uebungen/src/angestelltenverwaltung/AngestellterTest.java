package angestelltenverwaltung;

public class AngestellterTest {

	public static void main(String[] args) {
	Angestellter ang1 = new Angestellter();
	Angestellter ang2 = new Angestellter();
	
	ang1.setName("Meier");
	ang1.setGehalt(4500);
	ang2.setName("Peterson");
	ang2.setGehalt(6000);
	
	System.out.println("Name: " + ang1.getName());
	System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
	System.out.println("Name: " + ang2.getName());
	System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
		
	}
}
