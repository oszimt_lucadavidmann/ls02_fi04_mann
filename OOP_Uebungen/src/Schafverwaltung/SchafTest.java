package Schafverwaltung;

public class SchafTest {

	public static void main(String[] args)
	 {
	 System.out.println("\n--- Instanziere objekt1 ---");
	 Schaf objekt1 = new Schaf();
	 System.out.println("\n--- Instanziere objekt2 ---");
	 Schaf objekt2 = new Schaf("Othello");
	 System.out.println("\n--- Instanziere objekt3 ---");
	 Schaf objekt3 = new Schaf("Cloud", (byte)4, 2.15F);
	 System.out.println();
	 objekt1.merkmaleAusgeben("objekt1");
	 objekt2.merkmaleAusgeben("objekt2");
	 objekt3.merkmaleAusgeben("objekt3");
	 }
	
}
