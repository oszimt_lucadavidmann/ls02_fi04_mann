package Schafverwaltung;

public class Schaf {

	private String name;
    private byte alter;
    private float wollMenge;
    
    public Schaf() {
        System.out.println("Im parameterlosen Konstruktor 1");
    }
    
    public Schaf(String name) {
        this.name = name;
        System.out.println("Konstruktor 2");
    }
    
    public Schaf(String name, byte alter, float wolle) {
        this(name);
        this.alter = alter;
        this.wollMenge = wolle;
        System.out.println("Konstruktor 3");
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public byte getAlter() {
        return alter;
    }
    
    public void setAlter(byte alter) {
        this.alter = alter;
    }
    
    public float getWollMenge() {
        return wollMenge;
    }
    
    public void setWollMenge(float wollMenge) {
        this.wollMenge = wollMenge;
    }
    
    public void merkmaleAusgeben(String objektName)
    {
    System.out.println("\n- Ausgabe der Merkmale von " + objektName + " -");
    System.out.println("Name: " + name);
    System.out.println("Alter: " + alter + " Jahre");
    System.out.println("Wollmenge: " + wollMenge + " m^2");
    }

}
